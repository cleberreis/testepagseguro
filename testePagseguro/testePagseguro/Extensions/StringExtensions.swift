//
//  StringExtensions.swift
//  testePagseguro
//
//  Created by Cleber Reis on 06/11/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import Foundation

extension String{
    var asUrl: URL?{
        return URL(string: self)
    }
}
