//
//  UIImageViewExtensions.swift
//  testePagseguro
//
//  Created by Cleber Reis on 05/11/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(url: String) {
        guard let url = URL(string: url) else {return}
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
