//
//  Beer.swift
//  testePagseguro
//
//  Created by Cleber Reis on 31/10/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import Foundation

struct Beer: Codable {
    var imageUrl: String
    var name: String
    var abv: Double
    var tagline: String
    var ibu: Double?
    var description: String
    
    enum CodingKeys: String, CodingKey {
        case imageUrl = "image_url"
        case name
        case abv
        case tagline
        case ibu
        case description
    }
    
}
