//
//  BeerProvider.swift
//  testePagseguro
//
//  Created by Cleber Reis on 31/10/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import Foundation

struct BeerProvider {
    var delegate: BeerProviderDelegate?
    
    static let baseURL: URL = URL(string: "https://api.punkapi.com/v2")!
    
    init(delegate: BeerProviderDelegate) {
        self.delegate = delegate
    }
    
    func getBeers(){
        
        URLSession.shared.dataTask(with: BeerProvider.baseURL.appendingPathComponent("beers")) { (data, response, error) in
            DispatchQueue.main.sync {
                guard let data = data,
                    let beersArray = try? JSONDecoder().decode([Beer].self, from: data) else {
                        self.delegate?.onFailure()
                        return
                }
                self.delegate?.onSuccess(beers: beersArray)
            }
        }.resume()
    }
}

protocol BeerProviderDelegate {
    func onSuccess(beers: [Beer])
    func onFailure()
}
