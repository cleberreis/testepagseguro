//
//  ViewController.swift
//  testePagseguro
//
//  Created by Cleber Reis on 31/10/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import UIKit
import SDWebImage
import Hero
import Lottie

class BeerListViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var beersArray: [Beer] = []
    lazy var beerProvider: BeerProvider = BeerProvider(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableview.delegate = self
        tableview.dataSource = self
        
        tableview.backgroundView = UIView()
        
//        let animationView = LOTAnimationView(name: "beer_bubbles")
//        self.tableview.backgroundView?.addSubview(animationView)
//        animationView.play{ (finished) in
//            // Do Something
//        }
        
        beerProvider.getBeers()
        
        self.navigationItem.title = "Listagem de Cervejas"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let indexpath = tableview.indexPathForSelectedRow{
            let cell = tableview.cellForRow(at: indexpath) as? BeerTableViewCell
            cell?.imgBeer.hero.id = nil
            cell?.lblBeer.hero.id = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsBeer",
            let senderRow = sender as? Int,
            let destination = segue.destination as? DetailsBeerViewController{
            
            destination.beer = beersArray[senderRow]
            
        }
    }

}

extension BeerListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentBeer = beersArray[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? BeerTableViewCell else{return UITableViewCell()}
//        cell.imgBeer.load(url:currentBeer.imageUrl)
        cell.imgBeer.sd_setImage(with:currentBeer.imageUrl.asUrl, placeholderImage:#imageLiteral(resourceName: "beer"))
        cell.lblBeer.text = currentBeer.name
        cell.lblAbv.text = "\(currentBeer.abv)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? BeerTableViewCell
        cell?.imgBeer.hero.id = "imageBeer"
        cell?.lblBeer.hero.id = "nameBeer"
        
        performSegue(withIdentifier: "detailsBeer", sender: indexPath.row)
        
    }
    
}

extension BeerListViewController: BeerProviderDelegate{
    func onSuccess(beers: [Beer]) {
        self.beersArray = beers
        self.tableview.reloadData()
        self.tableview.backgroundView = UIView()
    }
    
    func onFailure() {
        let alert = UIAlertController(title: "Erro", message: "Deu algum erro.", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(actionOk)
        self.present(alert, animated: true)
        
    }
    
    
}
