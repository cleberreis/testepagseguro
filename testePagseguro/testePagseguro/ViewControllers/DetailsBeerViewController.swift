//
//  DetailsBeerViewController.swift
//  testePagseguro
//
//  Created by Cleber Reis on 06/11/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import UIKit
import SDWebImage

class DetailsBeerViewController: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblNome: UILabel!
    @IBOutlet weak var lblTagline: UILabel!
    @IBOutlet weak var lblAbv: UILabel!
    @IBOutlet weak var lblIbu: UILabel!
    @IBOutlet weak var lblDescricao: UILabel!
    
    var beer: Beer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgView.sd_setImage(with: beer.imageUrl.asUrl, placeholderImage:#imageLiteral(resourceName: "beer"))
        lblNome.text = beer.name
        lblTagline.text = "Teor Alcoólico: " + beer.tagline
        lblAbv.text = "Escala de amargor: \(beer.abv)"
        lblIbu.text = "Teor Alcoólico: \(beer.ibu ?? 00)"
        lblDescricao.text = beer.description
        
        self.navigationItem.title = beer.name
        
        imgView.layer.borderWidth = 0.5
        imgView.layer.masksToBounds = false
        imgView.layer.borderColor = UIColor.gray.cgColor
        imgView.layer.cornerRadius = imgView.frame.height/2
        imgView.clipsToBounds = true
    }


}
