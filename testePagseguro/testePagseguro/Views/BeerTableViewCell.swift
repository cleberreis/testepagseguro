//
//  BeerTableViewCell.swift
//  testePagseguro
//
//  Created by Cleber Reis on 05/11/18.
//  Copyright © 2018 Cleber Reis. All rights reserved.
//

import UIKit

class BeerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgBeer: UIImageView!
    @IBOutlet weak var lblBeer: UILabel!
    @IBOutlet weak var lblAbv: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
